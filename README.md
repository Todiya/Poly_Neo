Part 1:
The task is to load CSV to variable.
CSV is set of coefficients of several polynomials.
         Polynomial format:
                      Ax2 + Bx + C = 0
Part 2:
Sanitize CSV
Recognize and ignore invalid rows.

Part 3:
Computation
Use values extracted from CSV for basic data manipulation.
Compute ∆ using formula:
                   ∆ = b2 − 4ac
And then Zeros of a function

Part 4:
Data aggregation
Next step is to create CSV variable in which polynomial and its corresponding
computed values should be aggregated.

Part 5:
Saving output
Final task is to create output file and save table
                      
                      