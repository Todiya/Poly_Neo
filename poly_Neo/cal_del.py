import csv
from math import sqrt


def calculate_solution():
    ifile = open('file_without_blanks.csv', "r")
    reader = csv.reader(ifile)
    efile = open('file_with_solution.csv', "w", newline='')
    writer = csv.writer(efile)
    rownum = 0
    writer.writerow([' X^2=A', 'X=B', 'C', 'Delta', 'root1', 'root2'])
    for row in reader:
        if rownum == 0:
            header = row  # work with header row if you like
        else:
            colnum = 0
            arr = row[0].split(";")
            A = float(arr[0])
            B = float(arr[1])
            C = float(arr[2])
            delta = (B ** 2) - (4 * A * C)
            delta1 = sqrt(delta)
            r1 = (-B - delta1)/(2 * A)
            r2 = (-B + delta1)/(2 * A)
            solutionsList = [(A, B, C, delta, r1, r2)]
            solutionsList = zip(*solutionsList)
            writer.writerow(solutionsList)
        rownum += 1
    ifile.close()
    efile.close()
    return efile
