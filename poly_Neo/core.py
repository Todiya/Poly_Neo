import csv
import os


lines_file = []


def read_file():
    if os.getenv('ALL_PATH'):
        with open(os.getenv('ALL_PATH'), "r") as my_file:
            lines_file = my_file.readlines()
    file = open('file_without_blanks.csv', "w", newline='')
    writer = csv.writer(file)
    writer.writerow([' X^2=A', 'X=B', 'C'])
    for line in lines_file:
        lineArr = line.strip(";")
        itemcount = 0
        for item in lineArr:
            if not item.isspace() and item.isnumeric():
                itemcount = itemcount + 1
        if itemcount == 3:
            file.write(line)
    print("the file is sanitized")
    file.close()
    return file, lines_file

