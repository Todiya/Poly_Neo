import poly_Neo


def run():
    lines_file = poly_Neo.read_file()
    poly_Neo.calculate_solution()


if __name__ == '__main__':
    run()
