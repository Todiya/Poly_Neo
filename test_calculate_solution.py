import os
from unittest import TestCase
import tempfile
import time


class TestCalculate_solution(TestCase):
    def test_check_solution(self):

        data = []
        if os.getenv('TEST_PATH'):
            with open(os.getenv('TEST_PATH')) as file:
                data = file.readlines()
            fd, temp_path = tempfile.mkstemp()
            with open(temp_path, 'w') as temp_file:
                for line in data:
                    temp_file.write(line)
            time.sleep(10)
            header = True
            with open(temp_path, 'r') as f:
                reader = f.readlines()
                for line in reader:
                    if header is not True:
                        variables_array = line.split(',')
                        clean_array = []
                        for elem in variables_array:
                            if ')"' not in elem:
                                clean_array.append(elem)

                        last_valid_array = []
                        for elem in clean_array:
                            clean_elem = elem.replace('"', "").replace("(", "").replace('(', "")
                            print(clean_elem)
                            try:
                                last_valid_array.append(float(clean_elem))
                            except:
                                pass
                        old_delta = last_valid_array[3]
                        new_delta = (last_valid_array[1] ** 2 - 4 * last_valid_array[0] * last_valid_array[2])
                        self.assertEqual(old_delta, new_delta)
                        print("delta matches")
                    else:
                        header = False
            print('\n\n Test')

            ####test clan
            os.remove(temp_path)


if __name__ == '__main__':
    TestCase.main()
