import tempfile
import unittest
from poly_Neo import core
import os


class TestStringMethods(unittest.TestCase):

    def test_read_clean_file(self):
        result = core.read_file()
        self.assertNotEqual(self, type(result), type([1, 1, 1]))
        if os.getenv('CORE_PATH'):
            with open(os.getenv('CORE_PATH')) as file:
                fd, temp_path = tempfile.mkstemp()
                data = file.readlines()
                for line in range(1, len(data)):
                    self.assertTrue(self, "foo" not in data[line])
                    self.assertTrue(self, "bar" not in data[line])
                    self.assertNotEqual(self, len(data[line]), 8)

                self.assertTrue(core.read_file())
            os.remove(temp_path)


if __name__ == '__main__':
    unittest.main()
